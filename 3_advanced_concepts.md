# Session 2: advanced concepts

## Part 1: Majoranas

After following this part you will be able to:

- Simulate a minimal model of Majorana devices
- Construct a lattice model from a continuum Hamiltonian
- Utilize the discrete symmetries of a system (time-reversal, particle-hole)


Majoranas are protected zero energy states that appear in some systems, and that useful for quantum computation.

Our goal is to simulate the most popular proposal for Majoranas—a Majorana nanowire.

![A sketch of a Majorana nanowire](images/majorana_sketch.png)

Its minimal model has the Hamiltonian

$$
H = (p^2/2m - \mu + \alpha p \sigma_y) \tau_z + \Delta(x) \tau_x + B_x \sigma_x
$$
with a parabolic normal dispersion, spin-orbit coupling, Zeeman field, and superconducting pairing (here position-dependent).
The Pauli matrices σ and τ act on spins and particle-hole degrees of freedom.

This Hamiltonian becomes topological when $B_x^2 > \Delta^2 + \mu^2$, so that the end of a finite nanowire develop Majoranas.

It's the most complicated Hamiltonian we had (so far 😈), and converting it to tight-binding i


For a warm-up let us obtain its dispersion, discretize it, and verify that we have no surprises.


## Discretizing Majorana band structure


Let's get started

```python
import numpy as np
from matplotlib import pyplot
import kwant
import kwant.continuum

import ipywidgets
from tqdm.notebook import tqdm

pi = np.pi
```

```python
def interact(function, **params):
    """A convenience function for varying parameters."""
    params_spec = {
        key: ipywidgets.FloatText(value=value, step=0.05)
        for key, value in params.items()
    }
    return ipywidgets.interactive(function, **params_spec)
```

```python
# Here's a demo of interact
def simple_plot(x, y):
    return pyplot.plot([0, x, 1], [0, y, 1])

interact(simple_plot, x=.5, y=.5)
```

Now let's define the system.

Notice that we write $H$ as a string

```python
majorana_hamiltonian = """
    (k_x**2 / 2 - mu) * kron(sigma_0, sigma_z)
    + alpha * k_x * kron(sigma_y, sigma_z)
    + Delta * kron(sigma_0, sigma_x)
    + B_x * kron(sigma_x, sigma_0)
"""

## A helper function for converting strings like this into evaluatable expressions.
h_cont = kwant.continuum.lambdify(majorana_hamiltonian)
```

```python
momenta = np.linspace(-4, 4, 100)

def plot_spectrum(**params):
    kwant.plotter.spectrum(h_cont, ('k_x', momenta), params=params)
    
interact(
    plot_spectrum,
    B_x=0,
    mu=1,
    alpha=0.5,
    Delta=0
)
```

We need to get a *tight-binding* model from continuum. A lot of work, but Kwant already has it implemented

```python
wire_template = kwant.continuum.discretize(majorana_hamiltonian)
```

```python
infinite_wire = kwant.wraparound.wraparound(wire_template).finalized()

def spectrum_discrete(**params):
    kwant.plotter.spectrum(
        infinite_wire,
        ('k_x', np.linspace(-np.pi, np.pi, 301)),
        params=params,
    )

interact(
    spectrum_discrete,
    B_x=0,
    mu=0.3,
    alpha=0.3,
    Delta=0,
)
```

## Spectrum in a finite wire

```python
finite_wire = kwant.Builder()

def interval_shape(x_min, x_max):
    def shape(site):
        return x_min <= site.pos[0] < x_max
    
    return shape

finite_wire.fill(wire_template, shape=interval_shape(0, 20), start=[10]);
```

```python
def spectrum_finite(**params):
    kwant.plotter.spectrum(
        finite_wire.finalized(),
        params=params,
        x=('B_x', np.linspace(0, 1)),
        show=False
    )
    pyplot.ylim(-.5, .5)

interact(
    spectrum_finite,
    mu=0.3,
    alpha=0.3,
    Delta=0.3,    
)
```

We discovered "[Majorana oscillations](https://www.google.com/search?q=majorana+oscillations+-neutrino)"!


## Discrete symmetries


The BdG Hamiltonian may have a time-reversal symmetry

$$
\mathcal{T} = \sigma_y K;\quad \mathcal{T}^{-1}H(B, k_x)\mathcal{T} = H(-B, -k_x),
$$

which only applies when $B=0$ (here $K$ is complex conjugation).

It **always** has a particle-hole symmetry

$$
\mathcal{P} = i \mathcal{T} \tau_y=i\sigma_y \tau_yK;\quad \mathcal{P}^{-1}H(k_x)\mathcal{P} = -H(-k_x),
$$


```python
sigma_0 = np.eye(2)
sigma_x = np.array(
    [[0, 1],
     [1, 0]]
)
sigma_y = np.array(
    [[0, -1j],
     [1j, 0]]
)
sigma_z = np.array(
    [[1, 0],
     [0, -1]]
)
```

```python
# Useful operators

phs = np.kron(sigma_y, sigma_y)
trs = np.kron(sigma_y, sigma_0)
eh = -np.kron(sigma_0, sigma_z)  # Charge conservation, only there if Delta=0
```

```python
finite_wire.particle_hole = phs
finite_wire.time_reversal = trs
wire_finalized = finite_wire.finalized()
```

```python
default = dict(
    B_x=0.04,
    Delta=.3,
    Delta_lead=0,
    mu=.3,
    mu_lead=.3,
    alpha=.3,
)
```

```python
sym = wire_finalized.discrete_symmetry()
sym.validate(wire_finalized.hamiltonian_submatrix(
    params={**default, 'B_x': 0}
))
```

We therefore **confirm** that the Hamiltonian has the symmetries that we expect.
Let's see how to use them.

<!-- #region -->
## Andreev conductance


![Electron reflects from a superconductor as a hole and leaves a Cooper pair](https://upload.wikimedia.org/wikipedia/commons/0/0d/Andreev_reflection.svg)

we need to compute

$$
G_\textrm{NS} = \frac{e²}{h}\left(N_e + ∑R_{he} - ∑R_{ee}\right)
$$
<!-- #endregion -->

Let's make the topological part infinite by converting it into another lead.

This way we don't need to care about the finite size effects, coupling to another Majorana, and reflection of electrons from the opposite end of the wire.

```python
barrier = kwant.Builder()

barrier.fill(
    wire_template.substituted(Delta='Delta_lead', mu='mu_barrier'),
    shape=interval_shape(0, 1),
    start=[0],
)

lead = kwant.Builder(
    symmetry=kwant.TranslationalSymmetry([1]),
    particle_hole=phs,
    conservation_law=eh,
)

lead.fill(
    wire_template.substituted(Delta='Delta_lead', mu='mu_lead'),
    shape=(lambda site: True),
    start=[0]
)

topological_lead = kwant.Builder(
    symmetry=kwant.TranslationalSymmetry([-1]),
    particle_hole=phs,
)

topological_lead.fill(
    wire_template,
    shape=interval_shape(-np.inf, np.inf),
    start=[0],
)

barrier.attach_lead(lead)
barrier.attach_lead(topological_lead)

kwant.plot(barrier)
barrier = barrier.finalized()
```

Let's first check that everything works as expected and particle-hole symmetry is applied.

```python
s = kwant.smatrix(barrier, params={"mu_barrier": 0.3, **default})
```

```python
np.round(s.data, 3)
```

```python
# Below in each tuple the first is the lead number in the order it was attached
# The second is the sector of a conserved quantity (electron vs hole)

r_ee = s.submatrix(lead_out=(0, 0), lead_in=(0, 0))
r_he = s.submatrix(lead_out=(0, 1), lead_in=(0, 0))
r_eh = s.submatrix(lead_out=(0, 0), lead_in=(0, 1))
r_hh = s.submatrix(lead_out=(0, 1), lead_in=(0, 1))

np.round(r_ee, 3)
```

```python
# Once we specified the particle-hole symmetry, Kwant automatically uses it to correctly choose the modes:

r_ee - r_hh.conj()[::-1, ::-1]
```

Now we are ready to compute the conductance

```python
def andreev_conductance(s):
    N_electrons = len(s.submatrix((0, 0), (0, 0)))
    R_ee = s.transmission((0, 0), (0, 0))
    R_he = s.transmission((0, 1), (0, 0))
    return N_electrons - R_ee + R_he

def compute_conductance(syst, energies, params):
    conductances = []
    for energy in energies:
        conductances.append(
            andreev_conductance(kwant.smatrix(
                syst,
                energy,
                params=params
            ))
        )
    return conductances
```

```python
energies = np.linspace(-.5, .5, 101)

def plot_conductance_infinite(**params):
    pyplot.plot(
        energies,
        compute_conductance(barrier, energies, params={**default, **params})
    )

interact(
    plot_conductance_infinite,
    B_x=0,
    Delta=.1,
    mu=.5,
    mu_lead=.5,
    mu_barrier=.5,
    alpha=.1,
)
```

That's it! We observed the quantized Majorana peak!

To do it, we needed to:

- Discretize a continuum model
- Use the discrete symmetries (particle-hole and charge conservation)
- Create a topological transition

NB: there are probably hundreds of theory papers that are written about this system. In many the numerical part doesn't get much harder than this.


## Part 2: topological insulators

After this lecture you will be able to:

- simulate a minimal model for a 2D topological insulator (the quantum spin Hall effect)
- plot band structure for a 2D system
- compute and plot densities and current densities 


In this lecture we will look at the famous BHZ model for the quantum spin Hall effect. The Hamiltonian reads

$$H = \epsilon(\mathbf{k}) + \begin{pmatrix}
h(\mathbf{k})&0\\
0&h^*(-\mathbf{k})
\end{pmatrix}
+H_\text{so}$$

where $\epsilon(\mathbf{k}) = C - D (k_x^2+k_y^2)$ and 

$$h(\mathbf{k}) = \begin{pmatrix}
M - B (k_x^2 + k_y^2)& A (k_x + i k_y)\\
A( k_x - i k_y)& -M + B(k_x^2 + k_y^2)
\end{pmatrix}\,.$$

The topology comes only from  $h(\mathbf{k})$ - $\epsilon(k)$ just gives rise to some trivial asymmetries. In particular, the system
becomes topological depending on the value of $M$

![image.png](images/qsh.png)

It might seem trivial that these states cannot scatter into each other, as they do live in uncoupled spin blocks. But, as we know they are protected by time-reversal symmetry, and I can add any time-reversal symmetry preserving
term hat couples the spins to the Hamiltonian. The simplest one is Dresselhaus spin-orbit and given by

$$H_\text{so} = \begin{pmatrix}
0&0&0&-\Delta\\
0&0&\Delta&0\\
0&\Delta&0&0\\
-\Delta&0&0&0
\end{pmatrix}
$$

The parameters of this model have been fitted to more detailed band structure calculations of HgTe ([arxiv:0801.0901](https://arxiv.org/abs/0801.0901)), and we will use these parameters here:

$A=364.5\,\mathrm{meV nm}$, $B=-686\,\mathrm{meV nm^2}$, $D=-513\,\mathrm{meV nm^2}$, $M=-10\,\mathrm{meV}$, $C=0$, $\Delta=1.6\,\mathrm{meV}$.

In this course we will deviate a bit from these: below you will see I choose $D=-200\,\mathrm{meV nm^2}$. The reason is rather trivial, in this case I can choose a coarser discretization and you won't have to wait long for the result (binder is not very fast)


### Discretizing the 2D topological insulator

```python
Gamma_so = [[0, 0, 0, -1],
            [0, 0, +1, 0],
            [0, +1, 0, 0],
            [-1, 0, 0, 0]]

hamiltonian = """
   + M * kron(sigma_0, sigma_z)
   - B * (k_x**2 + k_y**2) * kron(sigma_0, sigma_z)
   - D * (k_x**2 + k_y**2) * kron(sigma_0, sigma_0)
   + A * k_x * kron(sigma_z, sigma_x)
   - A * k_y * kron(sigma_0, sigma_y)
   + Delta * Gamma_so
"""

params_bhz=dict(A=364.5, B=-686, D=-200, M=-10, Delta=1.6, Delta_lead=0)
```

```python
hamiltonian = kwant.continuum.sympify(hamiltonian, locals=dict(Gamma_so=Gamma_so))
hamiltonian
```

```python
lattice_spacing = 10
template = kwant.continuum.discretize(hamiltonian, grid=lattice_spacing)
```

```python
print(template)
```

### Plotting the bulk band structure


First let us plot the band structure to orient ourselfs. We will take a cut for $k_y=0$

```python
infinite_bulk = kwant.wraparound.wraparound(template).finalized()
```

```python
momenta = np.linspace(-np.pi, np.pi, 101)

def spectrum_bulk_1d(A, B, D, M, Delta):
    params = dict(A=A, B=B, D=D, M=M, Delta=Delta)
    
    kwant.plotter.spectrum(infinite_bulk, ("k_x", momenta),
                           params=dict(k_y=0, **params));
    
interact(
    spectrum_bulk_1d,
    A=params_bhz['A'],
    B=params_bhz['B'],
    D=params_bhz['D'],
    M=params_bhz['M'],
    Delta=params_bhz['Delta'])
```

With kwant we can actually also plot the 2D band structure (although here it does look boring)

```python
momenta = np.linspace(-np.pi, np.pi, 41)

def spectrum_bulk_2d(A, B, D, M, Delta):
    params = dict(A=A, B=B, D=D, M=M, Delta=Delta)
    
    kwant.plotter.spectrum(infinite_bulk, ("k_x", momenta), ("k_y", momenta),
                           params=params);
    
interact(
    spectrum_bulk_2d,
    A=params_bhz['A'],
    B=params_bhz['B'],
    D=params_bhz['D'],
    M=params_bhz['M'],
    Delta=params_bhz['Delta'])
```

```python

```

### Setting up a finite system


We can use exactly the same procedures as in the first part to build a simple system with leads:

```python
W = 100
L = 100

def rectangle_shape(site):
    x, y = site.pos
    return 0 < y < W and 0 < x < L

def lead_shape(site):
    x, y = site.pos
    return 0 < y < W
```

```python
syst = kwant.Builder()
syst.fill(template, rectangle_shape, (lattice_spacing, lattice_spacing))

# We use a trick here and set a different value of Delta in the lead
lead = kwant.Builder(kwant.TranslationalSymmetry([-lattice_spacing, 0]),
                     conservation_law=np.diag([-1, -1, 1, 1]))
lead.fill(template.substituted(Delta="Delta_lead"), lead_shape, (0, lattice_spacing))

syst.attach_lead(lead)
syst.attach_lead(lead.reversed())

syst = syst.finalized()
kwant.plot(syst);
```

Now let us calculate the band structure of the leads to orient ourselves

```python
kwant.plotter.bands(syst.leads[0], params=params_bhz, show=False)
pyplot.ylim(-30, 40)
```

Here we see the edge states in the bulk gap. There is a small anti-crossing -- this is due to finite-size effects, the edge states from both sides are overlapping


Now let us compute the conductance of the system:

```python
energies = np.linspace(-20, 20, 21)
Gs = []

for en in tqdm(energies):
    smat = kwant.smatrix(syst, energy=en, params=params_bhz)
    Gs.append(smat.transmission(1, 0))
    
pyplot.plot(energies, Gs)
pyplot.ylabel("G [$e^2/h$]")
pyplot.xlabel("energy [meV]")
pyplot.ylim(0, 10)
```
The plot looks fine: a quantized conductance inside the gap (depending on the number of points computed we may see a small dip where the anticrossing is).

We do get a warning though if we have a finite spin-orbit coupling in the lead.

```python

```

### Plotting the scattering wave functions


So far we only considered the scattering matrix, i.e. using the information about the wave function in the leads. But as you heard in the first lecture, Kwant in principle always computes the full wave function!

```python
wave_funcs = kwant.wave_function(syst, energy=0, params=params_bhz)
wf = wave_funcs(0)
print(wf.shape)
```

Let's try some naive way of plotting the wave function:

```python
kwant.plotter.map(syst, np.abs(wf[0])**2)
```

No, this doesn't work, as the wave function has more entries (spin, band) than lattice sites. We need to sum over these. The easiest way is to use a Kwant operator.

```python
density = kwant.operator.Density(syst, np.eye(4))

kwant.plotter.map(syst, density(wf[0]));
kwant.plotter.map(syst, density(wf[1]));
```

We can also use a more complicated on-site operator to for example compute the spin polarization

```python
sz_density = kwant.operator.Density(syst, np.kron(sigma_z, sigma_0))

kwant.plotter.map(syst, sz_density(wf[0]), cmap='seismic', vmin=-0.012, vmax=0.012);
kwant.plotter.map(syst, sz_density(wf[1]), cmap='seismic', vmin=-0.012, vmax=0.012);
```

Kwant can also easily plot the current density for you

```python
J_0 = kwant.operator.Current(syst)
J_z = kwant.operator.Current(syst, np.kron(sigma_z, sigma_0))

kwant.plotter.current(syst, J_0(wf[0], params=params_bhz));
kwant.plotter.current(syst, J_0(wf[1], params=params_bhz));

kwant.plotter.current(syst, J_z(wf[0], params=params_bhz));
kwant.plotter.current(syst, J_z(wf[1], params=params_bhz));
```

```python

```

### Adding disorder to the system


What we did so far was actually rather boring - we had a perfect system. What you will do in the exercises is to make edges imperfect, then you will see more structure. 

Here we'll go another way, and will add disorder to the system.

```python
hamiltonian = """
   + M * kron(sigma_0, sigma_z)
   - B * (k_x**2 + k_y**2) * kron(sigma_0, sigma_z)
   - D * (k_x**2 + k_y**2) * kron(sigma_0, sigma_0)
   + A * k_x * kron(sigma_z, sigma_x)
   - A * k_y * kron(sigma_0, sigma_y)
   + Delta * Gamma_so
   + V_dis(site, V_0, salt) * kron(sigma_0, sigma_0)
"""
```

```python
hamiltonian = kwant.continuum.sympify(hamiltonian, locals=dict(Gamma_so=Gamma_so))
hamiltonian
```

```python
lattice_spacing = 5
template = kwant.continuum.discretize(hamiltonian, grid=lattice_spacing)
```

```python
print(template)
```

```python
syst = kwant.Builder()
syst.fill(template, rectangle_shape, (lattice_spacing, lattice_spacing))

# We use a trick here and set a different value of Delta in the lead
lead = kwant.Builder(kwant.TranslationalSymmetry([-lattice_spacing, 0]),
                     conservation_law=np.diag([-1, -1, 1, 1]))
lead.fill(template.substituted(Delta="Delta_lead"), lead_shape, (0, lattice_spacing))

syst.attach_lead(lead)
syst.attach_lead(lead.reversed())

syst = syst.finalized()

# Redefine the operators for the new system
sz_density = kwant.operator.Density(syst, np.kron(sigma_z, sigma_0))
J_0 = kwant.operator.Current(syst)
J_z = kwant.operator.Current(syst, np.kron(sigma_z, sigma_0))
```

We had a random on-site disorder drawn out of the uniform distribution $[-V_0/2, V_0/2]$

```python
def V_dis(site, V_0, salt):
    return V_0 * (kwant.digest.uniform(site.tag, salt) - 0.5)
```

This is how the disorder looks like

```python
kwant.plotter.map(syst, lambda i: syst.hamiltonian(i, i, params=dict(V_dis=V_dis, V_0=1, salt="13", **params_bhz))[0,0].real);
```

Now do the wave function plot with disorder

```python

```

```python
def disordered_wf(V_0):
    wave_funcs = kwant.wave_function(syst, energy=-5, params=dict(V_dis=V_dis, V_0=V_0, salt="13", **params_bhz))
    wf = wave_funcs(0)

    kwant.plotter.map(syst, sz_density(wf[0]), cmap='seismic', vmin=-0.012, vmax=0.012);
    kwant.plotter.map(syst, sz_density(wf[1]), cmap='seismic', vmin=-0.012, vmax=0.012);
    
interact(
    disordered_wf,
    V_0=50)
```

```python
def disordered_currents(V_0):
    wave_funcs = kwant.wave_function(syst, energy=-5, params=dict(V_dis=V_dis, V_0=V_0, salt="13", **params_bhz))
    wf = wave_funcs(0)

    kwant.plotter.current(syst, J_0(wf[0], params=params_bhz), vmax=0.04);
    kwant.plotter.current(syst, J_0(wf[1], params=params_bhz), vmax=0.04);    

interact(
    disordered_currents,
    V_0=50)
```
